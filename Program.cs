using System;

namespace Spanglish
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sp;
            string[] en;
            string p1s="", p2s = "", p1e="",p2e="";
            int spa, eng;
            Console.WriteLine("¿Cuantas palabras en Español?");
            spa = Convert.ToInt16(Console.ReadLine());
            sp = new string[spa];
            Console.WriteLine("¿Cuantas palabras en Ingles?");
            eng = Convert.ToInt16(Console.ReadLine());
            en = new String[eng];
            for (int i = 1; i <= sp.Length; i++)
            {
                Console.WriteLine("Inserta la palabra {0} en español: ",(i));
                sp[i-1] = Console.ReadLine();
            }
            for (int i = 1; i <= en.Length; i++)
            {
                Console.WriteLine("Inserta la palabra {0} en ingles: ",(i));
                en[i - 1] = Console.ReadLine();
            }
            for (int i = 0; i < en.Length; i++)
            {
                  p1s = sp[i].Substring(startIndex:0,length:(sp[i].Length/2));
                  p2s = sp[i].Substring(startIndex:(sp[i].Length/2),length:((sp[i].Length) - (sp[i].Length/2)));
                  p1e = en[i].Substring(startIndex:0,length:(en[i].Length/2));
                  p2e = en[i].Substring(startIndex:(en[i].Length/2),length:((en[i].Length) - (en[i].Length/2)));
                  Console.WriteLine("Parte 1 en español con parte 2 en ingles: {0}",(p1s + p2e));
                  Console.WriteLine("Parte 1 en ingles con parte 2 en español: {0}",(p1e + p2s));
                  Console.WriteLine("Parte 2 en ingles con parte 1 en español: {0}",(p2e + p1s));
                  Console.WriteLine("Parte 2 en español con parte 1 en ingles: {0}",(p2s + p1e));
            }
        }
    }
}
